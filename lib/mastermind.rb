require 'byebug'
class Code

  PEGS = {
        "B" => :blue,
        "G" => :green,
        "O" => :orange,
        "P" => :purple,
        "R" => :red,
        "Y" => :yellow
        }

  attr_reader :pegs

  def self.parse(string)
     raise "wrong length" unless string.length == 4
    pegs = string.split('').map! do |letter|
      raise "parse error" unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.sample }
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(code)
    matches = 0
    pegs.each_index do |idx|
      if code.pegs[idx] == pegs[idx]
        matches += 1
      end
    end
    matches
  end

  def near_matches(code)
    near = 0
    arg = code.color_count
    self.color_count.each do |color, amount|
      next unless arg.has_key?(color)
      near += [amount, arg[color]].min
    end
    near - self.exact_matches(code)
  end

  def color_count
    color_count = Hash.new(0)
      @pegs.each do |color|
        color_count[color] += 1
      end
    color_count
  end

  def ==(code)
    return false unless code.is_a?(Code)
    pegs == code.pegs ? true : false
  end

end

class Game

  attr_reader :secret_code, :turns

  def initialize(secret_code = Code.random, turns = 10)
    @secret_code = secret_code
    @turns = turns
  end

  def get_guess
    puts "type a guess and press enter"
    begin
    Code.parse(gets.chomp)
    rescue
      puts "Invalid entry, type 4 letters from the key with no spaces"
      retry
    end
  end

  def play
    Code::PEGS.each {|letter, color| puts "#{letter} for #{color}"}
    tries = turns
    turns.times do
      guess = get_guess
      tries -= 1
      if guess == secret_code
        puts "nice, you got it on your #{turns - tries} attempt"
        return
      else
        display_matches(guess)
        puts "You have #{tries} tries left"
      end
    end
  end

  def display_matches(code)
    puts "You got #{secret_code.exact_matches(code)} exact matches"
    puts "You got #{secret_code.near_matches(code)} near matches"
  end

end

if $PROGRAM_NAME == __FILE__
  Game.new(Code.parse('bbbb'), 20).play
end
